using System;

namespace Api.Localization
{
    /// <summary>
    /// Indicates a property is localizable.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class LocalizablePropertyAttribute : Attribute
    {
        public string Key { get; set; }
    }
}