using System.Collections.Generic;

namespace Api.Localization
{
    public static class LocalizationCodes
    {
        public static string Default => "en-uk";
        
        public static readonly IReadOnlyCollection<string> Codes = new[]
        {
            "nl-be",
            "en-uk",
            "no"
        };
    }
}