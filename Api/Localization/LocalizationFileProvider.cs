using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Api.Localization
{
    public static class LocalizationFileProvider
    {
        private static Dictionary<string, IConfiguration> _languageConfiguration = new Dictionary<string, IConfiguration>();

        public static IConfiguration GetLocalizations(string countryCode)
        {
            if (_languageConfiguration.ContainsKey(countryCode))
            {
                return _languageConfiguration[countryCode];
            }
            
            var newLanguageConfigurationFile = new ConfigurationBuilder()
                .AddJsonFile($"Localization/Resources/{countryCode}.json", false, true)
                .Build();
            
            _languageConfiguration.Add(countryCode, newLanguageConfigurationFile);

            return newLanguageConfigurationFile;
        }
    }
}