using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using Api.Error;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Api.Localization
{
    /// <summary>
    ///  Tries to translate any properties on objects returned from actions which are annotated with the LocalizableProperty.
    /// </summary>
    public class LocalizationFilter : IActionFilter
    {
        private readonly ILocalizationService _localizationService;

        public LocalizationFilter(IHttpContextAccessor contextAccessor)
        {
            var language =
                contextAccessor.HttpContext.Items[LocalizationLanguageResolvingMiddleware.LocalizationLanguage] as string;
            
            _localizationService = new LocalizationService(language);
        }

        /// <summary>
        /// Checks whether an action completed successfully and translates the properties.
        /// </summary>
        /// <param name="context">ActionExecutedContext</param>
        /// <exception cref="ApiException">Thrown when the action completed with an exception.</exception>
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception != null)
            {
                var localizedMessage = _localizationService.GetTranslatedValue("Error.Technical");
                throw new LocalizationException(localizedMessage ?? context.Exception.Message, context.Exception);
            }
            
            var result = context.Result as ObjectResult 
                         ?? throw new NotSupportedException($"Cannot translate {context.Result.GetType()}");

            TranslateProperties(result);
        }

        private void TranslateProperties(ObjectResult result)
        {
            var resultData = result.Value;
            var resultType = resultData.GetType();

            if (!resultType.IsPrimitive)
            {
                if (resultData is IEnumerable resultList)
                {
                    var enumerator = resultList.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        var item = enumerator.Current;

                        if (item != null)
                        {
                            TranslateObjectProperties(item.GetType(), item);
                        }
                    }
                }
                else
                {
                    TranslateObjectProperties(resultType, resultData);
                }
            }
        }

        /// <summary>
        /// Checks properties of an object and checks if the property is annotated with the LocalizeableProperty attribute.
        /// A property is considered translatable if it's a string/enum.
        /// Once an attribute is found the LocalizationService is used to try a fetch the translated value.
        ///
        /// If the attribute has no 'Value' set manually the property name is used as translation key.
        /// </summary>
        /// <param name="resultType">The type of the action result object</param>
        /// <param name="resultData">The action result object</param>
        private void TranslateObjectProperties(Type resultType, object resultData)
        {
            var properties = resultType.GetProperties();

            if (properties.Any())
            {
                foreach (var property in properties)
                {
                    if (property.PropertyType == typeof(string) || property.PropertyType.IsEnum)
                    {
                        var localizationAttribute = property.GetCustomAttribute<LocalizablePropertyAttribute>();

                        if (localizationAttribute != null)
                        {
                            var key = localizationAttribute.Key ?? property.Name;

                            property.SetValue(resultData, _localizationService.GetTranslatedValue(key));
                        }
                    }
                }
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Nothing to do here.
        }
    }
}