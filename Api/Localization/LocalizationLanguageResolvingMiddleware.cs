using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Api.Localization
{
    public class LocalizationLanguageResolvingMiddleware : IMiddleware
    {
        public const string LocalizationLanguage = "LocalizationLanguage";
        
        private const string LanguageHeader = "Accept-Language";
        private const string LanguageQuery = "lang";

        public Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var language = LocalizationCodes.Default;

            var queryParameters = context.Request.Query;
            if (queryParameters.ContainsKey(LanguageQuery))
            {
                language = queryParameters[LanguageQuery];
            }
            
            var headers = context.Request.Headers;
            if (headers.ContainsKey(LanguageHeader))
            {
                language = headers[LanguageHeader];
            }

            if (!LocalizationCodes.Codes.Contains(language))
            {
                language = LocalizationCodes.Default;
            }
            
            context.Items.Add(LocalizationLanguage, language);

            return next(context);
        }
    }
}