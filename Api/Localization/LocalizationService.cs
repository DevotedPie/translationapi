using Microsoft.Extensions.Configuration;

namespace Api.Localization
{
    public interface ILocalizationService
    {
        public string GetTranslatedValue(string key);
    }
    
    /// <summary>
    /// Class that reads a translation file for a certain language and is used to get a translation based on a key. 
    /// </summary>
    public class LocalizationService : ILocalizationService
    {
        private readonly IConfiguration _languageConfiguration;

        public LocalizationService(string countryCode)
        {
            _languageConfiguration = LocalizationFileProvider.GetLocalizations(countryCode);
        }

        public string GetTranslatedValue(string key)
        {
            return _languageConfiguration[key];
        }
    }
}